import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class MyWorld extends World
{
    public MyWorld()
    {    
        super(600, 600, 1); 
        showText("Click on a part to see the translation", 400, 580);
        
        addObject(new Neck(), 310, 520);
        addObject(new Ears(), 300, 375);
        addObject(new Face(), 300, 350);
        addObject(new Lips(), 300, 467);
        addObject(new Mouth(), 300, 465);
        addObject(new Tongue(), 295, 475);
        addObject(new Teeth(), 295, 455);
        addObject(new Nose(), 300, 370);
        addObject(new Eyes(), 300, 310);
        addObject(new Eyebrows(), 390, 230);
        addObject(new Hair(), 310, 220);
        
        int x = 80;
        int y = 570;
        int inc = 54;
        addObject(new Button("Neck", "", "Neck", "गरदन", "Gardan"), x, y); y -= inc;
        addObject(new Button("Ear", "", "Ear", "कान", "Kaan"), x, y); y -= inc;
        addObject(new Button("Face", "", "Face", "चेहरा", "Cheharaa"), x, y); y -= inc;
        addObject(new Button("Lip", "", "Lip", "होंठ", "Honth"), x, y); y -= inc;
        addObject(new Button("Mouth", "", "Mouth", "मुंह", "Munh"), x, y); y -= inc;
        addObject(new Button("Tongue", "", "Tongue", "जीभ", "Jeebh"), x, y); y -= inc;
        addObject(new Button("Teeth", "", "Teeth", "दांत", "Daant"), x, y); y -= inc;
        addObject(new Button("Nose", "", "Nose", "नाक", "Naak"), x, y); y -= inc;
        addObject(new Button("Eyes", "", "Eyes", "आंखें", "Aankhen"), x, y); y -= inc;
        addObject(new Button("Eyebrows", "", "Eyebrow", "भौंह", "Bhaunh"), x, y); y -= inc;
        addObject(new Button("Hair", "", "Hair", "बाल", "Baal"), x, y); y -= inc;
        
        addObject(new bg(), 500, 50);
    }
}
