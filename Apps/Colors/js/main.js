$( document ).ready( function()
{
    var words = [
        [ "लाल", "red" ],
        [ "काला", "black" ],
        [ "नारंगी", "orange" ],
        [ "नीला", "blue" ],
        [ "भूरा", "brown" ],
        [ "हरा", "green" ],
        [ "धूसर", "grey" ],
        [ "गुलाबी", "pink" ],
        [ "बैगनी", "purple" ],
        [ "सफ़ेद", "white" ],
        [ "पीला", "yellow" ],
    ];

    var colorHindi;
    var colorEnglish;
    var colorIndex;

    var totalCorrect = 0, totalQuestions = 0;
    function UpdateStats()
    {
        $( "#total-correct" ).html( "Total correct: " + totalCorrect );
        $( "#total-questions" ).html( "Questions answered: " + totalQuestions );
    }

    function IsCorrect()
    {
        $( ".letter-entry" ).css( "color", "#188c13" );
        totalCorrect += 1;
        $( "#message" ).html( "Correct!" );
        UpdateStats();
        PlaySound();
    }

    function IsWrong()
    {
        $( ".letter-entry" ).css( "color", "#c22d2d" );
        $( "#message" ).html( "WRONG!" );
        $( "#message" ).css( "color", "#c22d2d" );
        UpdateStats();
    }

    function PlaySound()
    {
        var audio = new Audio( "audio/" + colorHindi + ".mp3" );
        audio.play();
    }
    
    function NewQuestion()
    {
        $( "#next-question" ).hide();
        
        colorIndex = Math.floor( Math.random() * words.length );
        colorHindi = words[ colorIndex ][0];
        colorEnglish = words[ colorIndex ][1];
        console.log( colorHindi, colorEnglish );

        $( "#color" ).html( colorHindi );

        $( "#message" ).html( "Find the word for" );
        $( "#message" ).css( "color", "#333" );
    }

    $( ".answer" ).click( function() {
        var guess = $( this ).val();
        var correct = colorEnglish;

        totalQuestions += 1;

        if ( guess == correct )
        {
            IsCorrect();
            $( this ).css( "background", colorEnglish );
            if ( colorEnglish == "yellow" || colorEnglish == "white" || colorEnglish == "pink" )
            {
                $( this ).css( "color", "#000000" );
            }
            else
            {
                $( this ).css( "color", "#ffffff" );
            }

            // Remove this word from the list
            words.splice( colorIndex, 1 );
            console.log( words );
        }
        else
        {
            IsWrong();
        }

        if ( words.length > 0 )
        {
            $( "#next-question" ).show();
        }
        else
        {
            $( "#message" ).html( "You got them all!" );
        }
        
        UpdateStats();
    } );

    $( "#next-question" ).click( function() {
        NewQuestion();
    } );

    NewQuestion();
    UpdateStats();
} );
